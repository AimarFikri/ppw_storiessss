from django.db import models
from django.core.validators import *
class Matkul(models.Model):
    nama_matkul=models.CharField(max_length=50)
    nama_dosen=models.CharField( max_length=50)
    jumlah_sks=models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(24)])
    deksripsi_matkul=models.TextField(blank=True,null=True)
    
    LIST_TAHUN = (
        ('Gasal 2020/2021','Gasal 2020/2021'),
        ('Genap 2020/2021','Genap 2020/2021'),
        ('Lainnya','Lainnya'),
    )

    tahun_semester=models.CharField(
        max_length = 100,
        choices = LIST_TAHUN,
        default = ' Genap 2020/2021')
    ruang_kelas=models.TextField(blank=True,null=True)

    def __str__(self):
        return self.nama_matkul
