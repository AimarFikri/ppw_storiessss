from django.urls import path
from . import views



 
urlpatterns = [
    path('', views.forms, name='forms'),
    path('delete/<str:delete_id>', views.delete,name='delete'),
    path('matkul/<str:pk>',views.detail,name='detail')
]
