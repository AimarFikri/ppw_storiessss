from django import  forms

from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields=('nama_matkul','nama_dosen','jumlah_sks','deksripsi_matkul','tahun_semester','ruang_kelas', )
        labels={
            'nama_matkul':'Nama Mata Kuliah',
            'nama_dosen':'Nama Dosen',
        }
        widgets = {'nama_matkul' : forms.TextInput( attrs = {
        'class':'form-control ',
        'placeholder' : 'nama Matkul',
        }),
        'nama_dosen' : forms.TextInput( attrs = {
        'class':'form-control',
        'placeholder' : 'nama Dosen',
        }),
        'jumlah_sks' : forms.TextInput( attrs = {
        'class':'form-control',
        'placeholder' : 'Jumlah SKS',
        }),
        'deksripsi_matkul' : forms.Textarea( attrs = {
        'class':'form-control',
        'placeholder' : 'Deksripsi Matkul','rows':4, 'cols':15
        }),
        'tahun_semester' : forms.Select( attrs = {
        'class':'form-control',
        }),
        'ruang_kelas' : forms.TextInput( attrs = {
        'class':'form-control',
        'placeholder' : 'Ruang Kelas Gan',
        }),
        
    }