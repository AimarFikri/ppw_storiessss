from django.shortcuts import render, redirect
from django.views.generic import DetailView


from .forms import MatkulForm

from .models import Matkul

# Create your views here.
def forms(request):
    semua_matkul = Matkul.objects.all()
    matkul_form = MatkulForm(request.POST or None )
    if request.method == 'POST':
        if matkul_form.is_valid():
            matkul_form.save()
        
        return redirect('/forms/')

    context = {
        'matkul_form':matkul_form,
        'semua_matkul':semua_matkul,
    }
    return render(request, 'forms.html', context)

def delete(request,delete_id):
        matkul = Matkul.objects.get(id=delete_id)
        if request.method ==  "POST":
                matkul.delete()
                return redirect('/forms/')
        
        context = {'item':matkul}

def detail(request,pk):
        matkul = Matkul.objects.get(id=pk)
        
        context = {'item':matkul}
        return render(request,'matkul_detail.html',context)